var setPadding = function () {
    var padding = $(window).width() / 2 - 450;
    var windowH = $(window).height();
    if (windowH < 768) {
        $('section.home').css({
            'height': 'auto'
        });
        $('.baner-center').css({
            'paddingLeft': 0,
            'paddingRight': 0,
            'height': 'auto'
        });
    } else {
        $('section.home').css({
            'height': windowH
        });
        $('.baner-center').css({
            'paddingLeft': padding,
            'paddingRight': padding,
            'height': windowH
        });
    }
}

var generujBloczki = function(bloczki) {
    var html = '';
    for (var i in bloczki) {
        html += '<div class="col-md-4 col-sm-6"><div class="box"><h3>' + bloczki[i].title + '</h3><p>' + bloczki[i].body +  '</p></div></div>';
    }
    $('.insert-box').html(html);
}

function weryfikuj () {
    var kamil = /.*@.*\..{2,3}/;
    var pawel = /(\w+)@(\w){1,5}\.\w{2,3}/;
    var ewelina = /(\w+)|(\d+)@(.....)\.(\w{2,3})/;
    var rafal = /^([A-Z,a-z,0-9]){3,255}@([A-Z,a-z,0-9,\.]){2,255}\.[a-z]{2,5}$/;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var tab = [
        {
            e: 'j@j.pl',
            w: false
        },
        {
            e: 'test@test.pl',
            w: true
        },
        {
            e: 'jan.kowalski@fajna.firma.pl',
            w: true
        },
        {
            e: 'jan.kowalski@@fajna.firma.pl',
            w: false
        },
        {
            e: '![]test@asdlasdfasdfasdfafsd.pl',
            w: false
        },
        {
            e: 'test@p1l.p1',
            w: false
        }
    ];

    for (var i in tab) {
        console.log('>>>>>>> testujemy: ' + tab[i].e)
        if (kamil.test(tab[i].e) === tab[i].w) {
            console.log('kamil odpowiedział dobrze');
        } else {
            console.log('kamil odpowiedział źle');
        }
        if (pawel.test(tab[i].e) === tab[i].w) {
            console.log('pawel odpowiedział dobrze');
        } else {
            console.log('pawel odpowiedział źle');
        }
        if (ewelina.test(tab[i].e) === tab[i].w) {
            console.log('ewelina odpowiedział dobrze');
        } else {
            console.log('ewelina odpowiedział źle');
        }
        if (rafal.test(tab[i].e) === tab[i].w) {
            console.log('rafal odpowiedział dobrze');
        } else {
            console.log('rafal odpowiedział źle');
        }
        if (re.test(tab[i].e) === tab[i].w) {
            console.log('internet dobrze');
        } else {
            console.log('internet źle');
        }
    }
};

weryfikuj();

$(document).ready(function () {
    // baner
    var baner = $('.baner');
    baner.owlCarousel({
        'items': 1,
        'loop': true,
        'mouseDrag': false,
        'rewind': true,
        'dots': true,
        'autoplay': true,
        'autoplayTimeout': 3000,
        'autoplayHoverPause': false,
        'autoHeight': false
    });

    // bloczki

    // var xhr = new XMLHttpRequest();
    // xhr.onload = function () {
    //     if (xhr.status === 200) {
    //         generujBloczki(JSON.parse(xhr.responseText));
    //     }
    // };
    // xhr.open('GET', 'bloczki.json', true);
    // xhr.send(null);
    
    // $.getJSON('bloczki.json', function ( data ) {
    //     generujBloczki(data);
    // });
    
    axios.get('bloczki.json').then(function (response) {
        generujBloczki(response.data);
    });

    // formularz

    $('#enterEmail').on('submit', function (event) {
        event.preventDefault();
        
        var email = $('#enterEmail input[type="text"]').val();

        var emailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (emailValidation.test(email)) {
            axios.post('/form', {
                email: email
            }).then(function (response) {
                $('.alert-danger').show().text(response.data.wiadomosc);
            });
        } else {
            $('.alert-danger').show().text('Błędny adres email');
        }
    });

    // otwieranie / zamykanie menu
    $('button.menu').on('click', function () {
        $('nav').animate({
            left: 0
        }, 300);
    });

    $('nav button.zamknij').on('click', function () {
        $('nav').animate({
            left: -300
        }, 300);
    });

    // scroll do sekcji

    $('nav .list-group a').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var offset = $(href).offset();
        var body = $('body, html');
        body.stop().animate({scrollTop: offset.top}, '500');
    });

    // resize

    setPadding();

    $(window).on('resize', function () {
        setPadding();
    });
});