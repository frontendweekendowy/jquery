const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

var parser = bodyParser.urlencoded({extended: false});

app.use(express.static(path.join(__dirname, 'www')));

app.get('/', function (req, res) {
  res.sendFile('www/index.html')
})

app.post('/form', parser, function (req, res) {
  console.log(req.body);
  var odp = {
    status: false,
    wiadomosc: 'Brak adresu email'
  }
  if (req.body.email) {
    odp.status = true;
    odp.wiadomosc = 'Twój adres: ' + req.body.email + ' został zapisany';
    if (req.body.email === 'tajny@mail.pl') {
      odp.wiadomosc = 'Tajna wiadomość';
    }
  }
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(odp));
})

app.listen(3000, function () {
  console.log('Example app listening on porct 3000!')
})